defmodule CountersTest do
  use ExUnit.Case
  doctest Counters

  setup do
    Counters.clear()
    :ok
  end

  test "has no counters initially" do
    assert Counters.counters() == []
  end

  test "returns new counter name" do
    Counters.count("foo")
    assert Counters.counters() == ["foo"]
  end
end
