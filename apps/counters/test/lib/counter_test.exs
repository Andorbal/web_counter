defmodule CounterTest do
  use ExUnit.Case, async: true
  doctest Counters.Counter

  setup do
    {:ok, counter} = start_supervised {Counters.Counter, :foo}
    %{counter: counter}
  end

  test "returns 1 when started", %{counter: counter} do
    assert Counters.Counter.count(counter) == 1
  end

  test "increments after every call", %{counter: counter} do
    assert Counters.Counter.count(counter) == 1
    assert Counters.Counter.count(counter) == 2
    assert Counters.Counter.count(counter) == 3
  end
end
