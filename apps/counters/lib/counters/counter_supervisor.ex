defmodule Counters.CounterSupervisor do
  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Supervisor
  require Logger

  @registry_name :counter_process_registry

  @doc """
  Starts the supervisor.
  """
  def start_link, do: Supervisor.start_link(__MODULE__, [], name: __MODULE__)


  @doc """
  Will find the process identifier (in our case, the `counter_name`) if it exists in the registry and
  is attached to a running `RegistrySample.Account` process.
  If the `counter_name` is not present in the registry, it will create a new `Counter.Counter`
  process and add it to the registry for the given `counter_name`.
  Returns a tuple such as `{:ok, counter_name}` or `{:error, reason}`
  """
  def find_or_create_process(counter_name) do
    if counter_process_exists?(counter_name) do
      {:ok, counter_name}
    else
      counter_name |> create_counter_process
    end

    {:ok, whereis(counter_name)}
  end

  @doc """
  Determines if a `RegistrySample.Account` process exists, based on the `counter_name` provided.
  Returns a boolean.
  ## Example
      iex> RegistrySample.AccountSupervisor.account_process_exists?(6)
      false
  """
  def counter_process_exists?(counter_name) do
    case Registry.lookup(@registry_name, counter_name) do
      [] -> false
      _ -> true
    end
  end


  @doc """
  Creates a new account process, based on the `counter_name` integer.
  Returns a tuple such as `{:ok, counter_name}` if successful.
  If there is an issue, an `{:error, reason}` tuple is returned.
  """
  def create_counter_process(counter_name) do
    case Supervisor.start_child(__MODULE__, [via_tuple(counter_name)]) do
      {:ok, _pid} -> {:ok, counter_name}
      {:error, {:already_started, _pid}} -> {:error, :process_already_exists}
      other -> {:error, other}
    end
  end


  # registry lookup handler
  defp via_tuple(counter_name), do: {:via, Registry, {@registry_name, counter_name}}


  @doc """
  Returns the pid for the `counter_name` stored in the registry
  """
  def whereis(counter_name) do
    case Registry.lookup(@registry_name, counter_name) do
      [{pid, _}] -> pid
      [] -> nil
    end
  end

  @doc """
  Return a list of `counter_name` integers known by the registry.
  ex - `[1, 23, 46]`
  """
  def counters do
    Supervisor.which_children(__MODULE__)
    |> Enum.map(fn {_, counter_proc_pid, _, _} ->
      Registry.keys(@registry_name, counter_proc_pid)
      |> List.first
    end)
    |> Enum.sort
  end


  def clear do
    Supervisor.which_children(__MODULE__)
    |> Enum.map(fn {_, pid, _, _} ->
      Counters.Counter.stop(pid)
    end)
  end


  @doc false
  def init(_) do
    children = [
      worker(Counters.Counter, [], restart: :temporary)
    ]

    # strategy set to `:simple_one_for_one` to handle dynamic child processes.
    supervise(children, strategy: :simple_one_for_one)
  end
end
