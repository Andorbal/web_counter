defmodule Counters.Counter do
  use GenServer
  require Logger

  @registry_name :counter_process_registry

  @doc """
  Starts a new account process for a given `counter_name`.
  """
  def start_link(process_name) do
    GenServer.start_link(__MODULE__, 1, name: process_name)
  end


  def count(pid), do: GenServer.call(pid, :count)


  def stop(pid), do: GenServer.stop(pid)


  def handle_call(:count, _from, count) do
    {:reply, count, count + 1}
  end
end
