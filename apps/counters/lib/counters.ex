defmodule Counters do
  def count(name) do
    {:ok, pid} = Counters.CounterSupervisor.find_or_create_process(name)
    Counters.Counter.count(pid)
  end

  def counters, do: Counters.CounterSupervisor.counters()

  def clear, do: Counters.CounterSupervisor.clear()
end
